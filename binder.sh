#!/bin/bash
PCONFIG=$(mongo --quiet sfera --eval "printjson(db.config.findOne({'key': 'presenter_server_port'}, {_id:0}))")
PPORT=$(echo $PCONFIG| jq -r '.value')

if [ -z "$PPORT" ]
then
      PPORT=3000
fi

sudo iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-ports $PPORT
