var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mqtt = require('mqtt');
var client  = mqtt.connect('mqtt://localhost')
var SferaConf = require('./sferaconf');

client.on('connect', function () {
  client.subscribe('local/status');
})

client.on('message', function (topic, message) {
  let data = JSON.parse(message);
  if( data.event === 'sfera_status'){
    io.emit('status_update', message.toString() )
  }
})

app.use(express.static('assets'))

app.get('/', function(req, res){
  res.sendFile(__dirname + '/assets/index.html');
});

io.on('connection', function(socket){
  // console.log('a user connected');
  socket.on('disconnect', function(){
    // console.log('user disconnected');
  });
  socket.on('client_command', function(msg){
    let d = msg.split(':');
    client.publish(d[0], d[1]);
  });
});

SferaConf.getConf('presenter_server_port').then(
  (port) => {
    http.listen(port, function(){
      console.log('listening on *:%s', port);
    });
  },
  (err) => {
    console.log(err)
  }
);
